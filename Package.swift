// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.
// Copyright Modelar Technologies 2022

import PackageDescription

let package = Package(
    name: "ModelarScan",
    platforms: [.iOS(.v14)],
    products: [
        .library(
            name: "ModelarScan",
            targets: ["ModelarScan"]),
    ],
    dependencies: [
    ],
    targets: [
        .binaryTarget(
            name: "ModelarScan",
            url: "https://modelar.ai/files/ModelarScan-2.2.0.zip",
            checksum: "5c2a71a9a54f97a4c3ee5283fd4aae4b53dc6740f9aac3ac58a889fa626b3df9"),

    ]
)
