# Modelar Scan iOS SDK License 

The Modelar Scan iOS SDK is licensed for evaluation only. It cannot be redistributed on its own as part of any derivative work without express permission from Modelar Technologies. 

Copyright Modelar Technologies 2022.

