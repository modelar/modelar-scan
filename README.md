# Modelar Scan iOS SDK

This is a binary SPM distribution of the ModelarScan iOS library that encapsulates Modelar Technologies' real-time 3D scanning for iPhones with LiDAR.  

## Version notes

### 2.2.1

* Added THIRDPARTY.md for third party licences.

### 2.2

* Improved imagery collection and texture processing on refinement.

### 2.1

* Configurable snapshots: The snapshot resolution can now be set explicitly.

### 2.0

* Scan snapshots: A top-down snapshot of the scan is returned with the acceptedScanHandler.  This is a breaking change, requiring an additional argument be passed to the callback. 
* Configurable up axis: The up axis can now be changed from Y (the Apple default) to X or Z 
* Configurable origin: The scan can now be shifted so that (0, 0, 0) maps to the scan start (the default), the center of the floor plane, the absolute center, or the minimum bounds
* LiDAR density: the density of the LiDAR depth values acquired while scanning can now be set to "High", which creates a more dense point cloud and can help define features in the output model more sharply. 

### 1.0

Initial release. 
